var express = require('express');
var router = express.Router();
var mongoose = require('mongoose');


router
  .route('/')
  // get all projects
  .get(function(req, res, next) {
    mongoose.model('Project').find({}).populate('builder').populate('city').exec(function(err, projects) {
      if (err) {
        res.send(err);
      }
      res.json(projects);
    });
  })
  // insert new project
  .post(function(req, res) {

    var name = req.body.name;
    var builderId = req.body.builderId;
    var cityId = req.body.cityId;
    mongoose.model('Project').create({
      name : name,
      builder: builderId,
      city: cityId
    }, function(err, project) {
      if (err) {
        res.send("There was a problem adding the information to the database.");
      } else {
        console.log('POST creating new project: ' + project);
        res.json(project);
      }
    })
  });


router
  .route('/project/:id')
  // get project with given id
  .get(function(req, res, next) {
    mongoose.model('Project').findById(req.params.id).populate('builder').populate('city').exec(function(err, project) {
      if (err || !project) {
        console.log(req.params.id + ' was not found');
        res.status(404);
        var err = new Error('Not Found');
        err.status = 404;
        next(err);
      } else {
        console.log('GET Retrieving ID: ' + project._id);
        res.json(project);
      }
    });
  });


router
  .route('/search')
  .post(function(req, res, next) {
    var name = req.body.name;
    mongoose.model('Project').find({name:{ $regex: name , $options: 'i'}}).populate('builder').populate('city').exec(function(err, projects) {
      if (err) {
        res.send(err);
      }
      res.json(projects);
    });
  });

router
.route('/search/:name')
.get(function(req, res, next){
    var name = req.params.name;
    mongoose.model('Project').find({name:{ $regex: name , $options: 'i'}}).populate('builder').populate('city').exec(function(err, projects) {
      if (err) {
        res.send(err);
      }
      res.json(projects);
    });
});




module.exports = router;