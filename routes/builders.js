var express = require('express');
var router = express.Router();
var mongoose = require('mongoose');


router
  .route('/')
  // get all builders
  .get(function(req, res, next) {
    mongoose.model('Builder').find({}, function(err, builders) {
      if (err) {
        res.send(err);
      }
      res.json(builders);
    });
  })
  // insert new builder
  .post(function(req, res) {

    var name = req.body.name;
    mongoose.model('Builder').create({
      name : name
    }, function(err, builder) {
      if (err) {
        res.send("There was a problem adding the information to the database.");
      } else {
        console.log('POST creating new builder: ' + builder);
        res.json(builder);
      }
    })
  });


router
  .route('/builder/:id')
  // get builder with given id
  .get(function(req, res, next) {
    mongoose.model('Builder').findById(req.params.id, function(err, builder) {
      if (err || !builder) {
        console.log(req.params.id + ' was not found');
        res.status(404);
        var err = new Error('Not Found');
        err.status = 404;
        next(err);
      } else {
        console.log('GET Retrieving ID: ' + builder._id);
        res.json(builder);
      }
    });
  });


router
  .route('/search')
  .post(function(req, res, next) {
    var name = req.body.name;
    mongoose.model('Builder').find({name:{ $regex: name , $options: 'i'}}, function(err, builders) {
      if (err) {
        res.send(err);
      }
      res.json(builders);
    });
  });

router
.route('/search/:name')
.get(function(req, res, next){
    var name = req.params.name;
    mongoose.model('Builder').find({name:{ $regex: name , $options: 'i'}}, function(err, builders) {
      if (err) {
        res.send(err);
      }
      res.json(builders);
    });
});


module.exports = router;