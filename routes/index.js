var express             = require('express'),
    router              = express.Router(),
    session             = require('express-session'),
    passport            = require('../middlewares/facebook-login');


// initialize passposrt and and session for persistent login sessions
router.use(session({
    secret: "tHiSiSasEcRetStr",
    resave: true,
    saveUninitialized: true }));
router.use(passport.initialize());
router.use(passport.session());
 
// route middleware to ensure user is logged in, if it's not send 401 status
function isLoggedIn(req, res, next) {
    if (req.isAuthenticated())
        return next();
 
    res.sendStatus(401);
}


/* GET home login page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Real Estate Api Login' });
});


// send to facebook to do the authentication
router.get("/auth/facebook", passport.authenticate("facebook", { scope : "email" }));

// handle the callback after facebook has authenticated the user
router.get("/auth/facebook/callback",
    passport.authenticate("facebook", {
        successRedirect : "/content",
        failureRedirect : "/"
}));
 
 
// content page, it calls the isLoggedIn function defined above first
// if the user is logged in, then proceed to the request handler function,
// else the isLoggedIn will send 401 status instead
router.get("/content", isLoggedIn, function (req, res) {
    res.send("Congratulations! you've successfully logged in. <a href='/logout'>Logout</a>");
});
 
// logout request handler, passport attaches a logout() function to the req object,
// and we call this to logout the user, same as destroying the data in the session.
router.get("/logout", function(req, res) {
    req.logout();
    res.send("logout success!");
});


module.exports = router;
