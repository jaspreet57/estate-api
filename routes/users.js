var express = require('express');
var router = express.Router();
var mongoose = require('mongoose');


router
  .route('/')
  // get all users
  .get(function(req, res, next) {
    mongoose.model('User').find({}, function(err, users) {
      if (err) {
        res.send(err);
      }
      res.json(users);
    });
  })
  // insert new user
  .post(function(req, res) {

    var name = req.body.name;
    var email = req.body.email;
    mongoose.model('User').create({
      name : name,
      email : email
    }, function(err, user) {
      if (err) {
        res.send("There was a problem adding the information to the database.");
      } else {
        console.log('POST creating new user: ' + user);
        res.json(user);
      }
    })
  });



router
  .route('/user/:id')
  // get user with given id
  .get(function(req, res, next) {
    mongoose.model('User').findById(req.params.id, function(err, user) {
      if (err || !user) {
        console.log(req.params.id + ' was not found');
        res.status(404);
        var err = new Error('Not Found');
        err.status = 404;
        next(err);
      } else {
        console.log('GET Retrieving ID: ' + user._id);
        res.json(user);
      }
    });
  });


router
  .route('/search')
  .post(function(req, res, next) {
  var name = req.body.name;
    mongoose.model('User').find({name:{ $regex: name , $options: 'i'}}, function(err, users) {
      if (err) {
        res.send(err);
      }
      res.json(users);
    });
  });

router
.route('/search/:name')
.get(function(req, res, next){
  var name = req.params.name;
    mongoose.model('User').find({name:{ $regex: name , $options: 'i'}}, function(err, users) {
      if (err) {
        res.send(err);
      }
      res.json(users);
    });
});



module.exports = router;