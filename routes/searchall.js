var express = require('express');
var router = express.Router();
var mongoose = require('mongoose');
var asyncc = require('async');

router
.route('/:name')
.get(function(req, res, next){
  var name = req.params.name;
  
  var result ={};

  
  asyncc.parallel(
  [
    // get cities
    function(callback){
      mongoose.model('City').find({name:{ $regex: name , $options: 'i'}}, 'name').limit(5).exec(function(err, cities) {
        if (err) {
          callback(err);
        }
        result.City = cities;
        callback();
      });
    },
    // get projects
    function(callback){
       mongoose.model('Project').find({name:{ $regex: name , $options: 'i'}},'name').limit(5).exec(function(err, projects) {
       if (err) {
          callback(err);
        }
        result.Project = projects;
        callback();
      });
    },
    // get builders
    function(callback){
      mongoose.model('Builder').find({name:{ $regex: name , $options: 'i'}}, 'name').limit(5).exec(function(err, builders) {
         if (err) {
          callback(err);
        }
        result.Builder = builders;
        callback();
      });
    }
  ],
  function(err){
   if (err) {
        res.send(err);
      }
      res.json(result);
  }
  );
});



module.exports = router;