var express = require('express');
var router = express.Router();
var mongoose = require('mongoose');


router
  .route('/')
  // get all cities
  .get(function(req, res, next) {
    mongoose.model('City').find({}, function(err, cities) {
      if (err) {
        res.send(err);
      }
      res.json(cities);
    });
  })
  // insert new city
  .post(function(req, res) {

    var name = req.body.name;
    mongoose.model('City').create({
      name : name
    }, function(err, city) {
      if (err) {
        res.send("There was a problem adding the information to the database.");
      } else {
        console.log('POST creating new city: ' + city);
        res.json(city);
      }
    })
  });


router
  .route('/city/:id')
  // get city with given id
  .get(function(req, res, next) {
    mongoose.model('City').findById(req.params.id, function(err, city) {
      if (err || !city) {
        console.log(req.params.id + ' was not found');
        res.status(404);
        var err = new Error('Not Found');
        err.status = 404;
        next(err);
      } else {
        console.log('GET Retrieving ID: ' + city._id);
        res.json(city);
      }
    });
  });


router
  .route('/search')
  .post(function(req, res, next) {
    var name = req.body.name;
    mongoose.model('City').find({name:{ $regex: name , $options: 'i'}}, function(err, cities) {
      if (err) {
        res.send(err);
      }
      res.json(cities);
    });
  });

router
.route('/search/:name')
.get(function(req, res, next){
    var name = req.params.name;
    mongoose.model('City').find({name:{ $regex: name , $options: 'i'}}, function(err, cities) {
      if (err) {
        res.send(err);
      }
      res.json(cities);
    });
});


module.exports = router;