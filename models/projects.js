var mongoose = require('mongoose');

var projectSchema = new mongoose.Schema({
  name: String,
  builder: {type: mongoose.Schema.Types.ObjectId, ref: 'Builder'},
  city: {type: mongoose.Schema.Types.ObjectId, ref: 'City'}
});

mongoose.model ('Project', projectSchema);