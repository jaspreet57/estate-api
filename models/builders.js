var mongoose = require('mongoose');

var builderSchema = new mongoose.Schema({
  name: String
});

mongoose.model ('Builder', builderSchema);