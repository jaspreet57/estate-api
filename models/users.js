var mongoose = require('mongoose');

var userSchema = new mongoose.Schema({
  fbid: String,
  name: String,
  email: String,
  token: String
});

mongoose.model ('User', userSchema);