var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');

// Database connection setup
var db = require('./models/db');

//Define Models
var user = require('./models/users');
var city = require('./models/cities');
var builder = require('./models/builders');
var project = require('./models/projects');



// Define routes
var index = require('./routes/index');
var users = require('./routes/users');
var cities = require('./routes/cities');
var builders = require('./routes/builders');
var projects = require('./routes/projects');
var searchall = require('./routes/searchall');


//Define middlewares
var authenticate = require('./middlewares/authenticate');

// Express Setup
var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

// Use above degined routes here
app.use('/', index);


app.use(authenticate({salt : "qwerty"}));
app.use('/users', users);
app.use('/cities', cities);
app.use('/builders', builders);
app.use('/projects', projects);
app.use('/searchall', searchall);


// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.format({
      html: function(){
          res.render('error');
      },
      json: function(){
          res.json({message : err.status  + ' ' + err});
      }
  });
  
});

module.exports = app;
