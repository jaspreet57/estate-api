```
#######################
Note: 
#######################

All the four requirements are implemented in the given code. 
But as relationships between entities given in first requirement are not mentioned, I have assumed relationships on my own according to the problem statement.
My own assumed relationships may conflict with your understanding. So I am giving my assumptions below:

1. I have assumed that User have no relationship with other entities like Project, Builder and City. User is only for login purpose of the project and will be used as database for facebook login functionality.
2. Project, Builder and City are related to each other. Project-Builder has 1-1 relationship and Project-City has many-1 relationship.
3. All Entities have name as the only field for simplicity. Only Project Entity have other fields for maintaining relationships.
3. For facebook login functionality is applied to index route only and /content route is kept secure which can be accessible only if logged in with facebook.
4. Rest of the apis are secured with facebook login for simplicity. and Assignment also required so according to me.
5. All APIs are however secured using authentication of timestamp and signature as given in assignment.



#######################
required stack:
#######################


1. Nodejs and npm.
  follow https://nodejs.org/en/download/
  
2. Mongodb
    for Windows : get the community edition from https://www.mongodb.com/download-center#community
    for ubuntu and its derivatives : follow https://docs.mongodb.com/master/tutorial/install-mongodb-on-ubuntu/?_ga=2.184032612.331657357.1498078856-1158599696.1492850378

    (Note: No database setup is required in Mongodb for this app.)
  
3. Postman REST Client for testing REST APIs.
    Postman is google chrome extention app and can be found in chrome store. I am using Postman because of its pre-request script and environment features.
    
4. All other Dependencies will be installed along with source code.





#####################################################################
Steps to get the code, tools and run it on localhost: 
#####################################################################

1. clone the code from here https://gitlab.com/jaspreet57/estate-api
    git clone https://gitlab.com/jaspreet57/estate-api.git
      or
    download from the given url (if you don't have git installed)

2. Go to estate-api folder

3. Install all npm dependencies by running below command in terminal or command prompt
    npm install
    
4. To start the server, run below command
    npm start
    
    (note: after sccessfull start, Server will be running at port 3000, so app can be access at http://localhost:3000)




############################################################################################
Steps to test the app (assuming you are testing on http://localhost:3000 url)
############################################################################################

==> Facebook Login testing
1. This can be tested in any browser by opening root url i.e http://localhost:3000
2. click on Login with facebook link, which will open /content page after successfull login
    
    (Note: Facebook login can be tested in localhost only, because callback url in facebook app are configured for localhost only)
    
    
==> API testing
(Assuming you are using Postman REST client of Google Chrome, you need to do following settings in your Postman client for all the APIs calls)
1. set pre-request script as bellow code:

var salt = "qwerty";
var timestamp = new Date().getTime();
var url = request.url.split('?')[0];
var sigString = timestamp + salt + url;
var signature = CryptoJS.MD5(sigString);
postman.setGlobalVariable("timestamp", timestamp);
postman.setGlobalVariable("signature", signature);


2. append all APIs calls with below query string

  ?timestamp={{timestamp}}&signature={{signature}}

  for example http://localhost:3000/users?timestamp={{timestamp}}&signature={{signature}}
  
(Note: Above script and query string will work only in postman client, so I suggest to use that only. In case you want to use any other, then adjust the above two codes accordingly)



#######################
APIs
#######################

#### Requirement 1:

Insert/Get/Search apis for User, Project, Builder and City

User:
[get all users]
GET http://localhost:3000/users


[get user by id]
GET http://localhost:3000/users/user/:id


[create new user]
POST http://localhost:3000/users
{ name: "anyname", email: "anyemail" }


[For searching by name, both get and post APIs are implemented]
GET http://localhost:3000/users/search/:name
or
POST http://localhost:3000/users/search
{name : "query name"}


For Project, Builder and City, Same apis as above are implemented 
and can be tested by just changing users to projects, builders and cities
and user to project, builder and city



#### Requirement 2:
Requirement 2 is the authentication of each REST request using timestamp and signature.
Postman scripts are actually sending timestamp and signature to server already and are getting authenticated.
To test it you can change any of the following parameter:
1. For the current app Salt is kept as "qwerty". Change it in postman script to test it.
2. subtract 3000 miliseconds from the current timestamp to test if request older than 3 seconds are getting authenticated or not.
   change below line in postman script
   var timestamp = new Date().getTime();
   to
   var timestamp = new Date().getTime()-3000;
   
   

#### Requirement 3:
Search all by name can be tested using below query
GET http://localhost:3000/searchall/one

[Response will be]
{
      City: [Array of 5 top matching objects -  ObjectID and name],
      Project: [Array of 5 top matching objects - ObjectID and name],
      Builder: [Array of 5 top matching objects - ObjectID and name]
}

(Note: Searchall and search apis above are implement to search any substing in name also)



#### Requirement 4:
This is implemented as html page and not as REST API and is explained above.


```






















