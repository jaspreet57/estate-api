var passport            = require('passport'),
    FacebookStrategy    = require('passport-facebook').Strategy,
     mongoose = require('mongoose');


var facebookAuth = {
        'clientID'        : '277332782736377', // facebook App ID
        'clientSecret'    : 'd1d8bfa6ed8e6d7af13aaa18706eb206', // facebook App Secret
        'callbackURL'     : 'http://localhost:3000/auth/facebook/callback'
};


 
// passport needs ability to serialize and unserialize users out of session
passport.serializeUser(function (user, done) {
    done(null, user.id);
});
passport.deserializeUser(function (id, done) {
    mongoose.model('User').findById(id, function(err, user) {
        done(err, user);
    });
});


  
// passport facebook strategy
passport.use(new FacebookStrategy({
    "clientID"        : facebookAuth.clientID,
    "clientSecret"    : facebookAuth.clientSecret,
    "callbackURL"     : facebookAuth.callbackURL,
    "profileFields": ['id', 'name', 'emails']
},
function (token, refreshToken, profile, done) {
    mongoose.model('User').findOne({fbid: profile.id}, function(err, user) {
        if(!err && user){
          return done(null, user);
        }
        else if(err){
          return done(err, user);
        }
        else{
             mongoose.model('User').create({
                fbid  : profile.id,
                name :  profile.name.givenName + ' ' + profile.name.familyName,
                email : profile.emails[0].value,
                token : token
              }, function(err, user) {
                if (err) {
                  return done(err, user);
                } else {
                  return done(null, user);
                }
              });
        }
    });
}));



module.exports = passport;
