var CryptoJs = require('crypto-js');

module.exports = function(options) {
  return function(req, res, next) {
    if(req.query.timestamp && req.query.signature){
      
      // Comparing timestamp
      var serverTimestamp = new Date().getTime();
      
      var difference = (serverTimestamp - req.query.timestamp);
      // Calculating and comparing signature
      var url = req.get('host') + req.originalUrl;
      url = url.split('?')[0];
      var salt = options.salt;
      var signature = CryptoJs.MD5(req.query.timestamp+salt+url);
      console.log("url came as "+url);
      console.log("signature came is "+ req.query.signature);
      console.log("server signature is "+signature);
      console.log("timestamp came is "+ req.query.timestamp);
      console.log("difference in time is "+difference);
      
      
      if(signature == req.query.signature && difference <= 3000){
        next();
      }else{
        var err = new Error('Authentication failed');
        err.status = 401;
        next(err);
      }
    }else{
      var err = new Error('Authentication failed');
      err.status = 401;
      next(err);
    }
  }
}